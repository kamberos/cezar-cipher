﻿using System;

namespace Kod_Cezara
{
    class CipherTools
    {

        public static string Cipher(string input, int key)
        {
            string output = "";
            foreach (var item in input)
            {
                if (item >= 65 && item <= 90)
                {
                    if ((item + key) > 90)
                    {
                        char a = (char)((item + key) % 90);
                        output += (char) ( a + 64);
                    }
                    else
                    {
                        output += (char)(item + key);
                    }
                }
                else if (item >= 97 && item <= 122)
                {
                    if ((item + key) > 122)
                    {
                        char a = (char)((item + key) % 122);
                        output +=(char) (a + 96);
                    }
                    else
                    {
                        output += (char)(item + key);
                    }
                }
                else
                {
                    output += item;
                }
            }
            return output;

        }
        public static string Decipher(string DecipheringInput, int key)
        {
            string DecipheredOutput = "";
            foreach (var item in DecipheringInput)
            {
                if (item >= 65 && item <= 90)
                {
                    if ((item - key) < 65 )
                    {
                        char a = (char) ( 91 - (65 -(item - key)));
                        DecipheredOutput += a;
                    }
                    else
                    {
                        DecipheredOutput += (char) (item - key);
                    }
                }
                else if (item >= 97 && item <= 122)
                {
                    if ((item - key) < 97)
                    { 
                        char a = (char) ( 123 - (97 -(item - key)));
                        DecipheredOutput += a;
                    }
                    else
                    {
                        DecipheredOutput += (char) (item - key);
                    }
                }
                else
                {
                    DecipheredOutput += item;
                }
            }
            return DecipheredOutput;
        }

    }

    class Program
    {

        static void Main()
        {
            string input = "Ala!!@$ ma kota";
            int key = 3;
            while(key > 26) // if number is bigger than amount of alphabet letters 
            {
                key -= 26;
            }
            string Ciphered = CipherTools.Cipher(input, key);
            Console.WriteLine(input);
            Console.WriteLine(Ciphered);
            Console.WriteLine(CipherTools.Decipher(Ciphered, key));
        }
    }
}
